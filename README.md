# OptimizelyAlloy

Clean project with DB connection to local sql server

## Getting started

- Download repo
- Enter project OptimizelyAlloy
- Install the Episerver Templates
`dotnet new -i Episerver.Net.Templates --nuget-source https://nuget.optimizely.com/feed/packages.svc/ --force`
- Install the Episerver CLI Tool
`dotnet tool install Episerver.Net.CLI -g --add-source https://nuget.optimizely.com/feed/packages.svc/`
- Create clean database
`dotnet-episerver create-cms-database -S . -E -dn "OptimizelyAlloy" -du "adminuser" -dp "adminpassword" OptimizelyAlloy.csproj`
- Create CMS Admin
`dotnet-episerver add-admin-user -u AdminUser -p Admin123! -e admin@example.com -c EPiServerDB "OptimizelyAlloy.csproj"`
- Open in visual studio
- Run with IIS Express
- Go to url with sub-path /episerver/cms/ and login.
